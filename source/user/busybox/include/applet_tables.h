/* This is a generated file, don't edit */

#define NUM_APPLETS 50

const char applet_names[] ALIGN1 = ""
"[" "\0"
"[[" "\0"
"ash" "\0"
"brctl" "\0"
"cat" "\0"
"chpasswd" "\0"
"cp" "\0"
"date" "\0"
"dd" "\0"
"depmod" "\0"
"dmesg" "\0"
"echo" "\0"
"expr" "\0"
"free" "\0"
"grep" "\0"
"halt" "\0"
"ifconfig" "\0"
"init" "\0"
"init" "\0"
"insmod" "\0"
"kill" "\0"
"killall" "\0"
"login" "\0"
"ls" "\0"
"lsmod" "\0"
"mkdir" "\0"
"modprobe" "\0"
"mount" "\0"
"ping" "\0"
"poweroff" "\0"
"printf" "\0"
"ps" "\0"
"pwd" "\0"
"reboot" "\0"
"rm" "\0"
"rmmod" "\0"
"route" "\0"
"sed" "\0"
"sh" "\0"
"sleep" "\0"
"syslogd" "\0"
"telnetd" "\0"
"test" "\0"
"top" "\0"
"touch" "\0"
"udhcpc" "\0"
"udhcpd" "\0"
"umount" "\0"
"uptime" "\0"
"vconfig" "\0"
;

int (*const applet_main[])(int argc, char **argv) = {
test_main,
test_main,
ash_main,
brctl_main,
cat_main,
chpasswd_main,
cp_main,
date_main,
dd_main,
depmod_main,
dmesg_main,
echo_main,
expr_main,
free_main,
grep_main,
halt_main,
ifconfig_main,
init_main,
init_main,
insmod_main,
kill_main,
kill_main,
login_main,
ls_main,
lsmod_main,
mkdir_main,
modprobe_main,
mount_main,
ping_main,
halt_main,
printf_main,
ps_main,
pwd_main,
halt_main,
rm_main,
rmmod_main,
route_main,
sed_main,
ash_main,
sleep_main,
syslogd_main,
telnetd_main,
test_main,
top_main,
touch_main,
udhcpc_main,
udhcpd_main,
umount_main,
uptime_main,
vconfig_main,
};
const uint16_t applet_nameofs[] ALIGN2 = {
0x0000,
0x0002,
0x0005,
0x0009,
0x000f,
0x0013,
0x001c,
0x001f,
0x0024,
0x0027,
0x002e,
0x0034,
0x0039,
0x003e,
0x0043,
0x0048,
0x004d,
0x0056,
0x005b,
0x0060,
0x0067,
0x006c,
0x8074,
0x007a,
0x007d,
0x0083,
0x0089,
0x0092,
0x4098,
0x009d,
0x00a6,
0x00ad,
0x00b0,
0x00b4,
0x00bb,
0x00be,
0x00c4,
0x00ca,
0x00ce,
0x00d1,
0x00d7,
0x00df,
0x00e7,
0x00ec,
0x00f0,
0x00f6,
0x00fd,
0x0104,
0x010b,
0x0112,
};
#define MAX_APPLET_NAME_LEN 8
